class SemanticOctopus < Formula
  desc "Aggregate and reason on RDF, then publish it"
  homepage "http://www.opendatahacklab.org/semanticoctopus/"
  url "http://www.opendatahacklab.org/semanticoctopus/releases/semanticoctopus0.2.0.tgz"
  sha256 "d3b8ec074bcc736f733ed1ae165c5b7a9f898722019a99ed0ebd474b95f015f7"

  depends_on :java => "1.7+"

  def install
    libexec.install "semanticoctopus-0.2.0-jar-with-dependencies.jar"
    bin.write_jar_script libexec/"semanticoctopus-0.2.0-jar-with-dependencies.jar", "semocto"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! It's enough to just replace
    # "false" with the main program this formula installs, but it'd be nice if you
    # were more thorough. Run the test with `brew test semanticoctopus`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
